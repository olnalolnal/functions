const getSum = (str1, str2) => {
  if (typeof str1 != "string" || typeof str2 != "string" || isNaN(str1) || isNaN(str2)) return false;
  var x = (str1 === '' ? 0 : parseInt(str1));
  var y = (str2 === '' ? 0 : parseInt(str2));
  return (x + y).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var p = 0, c = 0;
  for (const x of listOfPosts) {
    if (x.author === authorName) p++;
    if (x.comments != undefined) {
      for (const y of x.comments) {
        if (y["author"] === authorName) c++;
      }
    }
  }
  return 'Post:' + p + ',comments:' + c;

};

const tickets = (people) => {
  var cash = [];
  var p = 1;
  for (const x of people) {
    if (x == 25) { cash.push(x); }
    else {
      if (x == 50) {
        let a = 0;
        for (let i = 0; i < cash.length; i++) {
          if (cash[i] == 25) {
            cash.splice(i, 1);
            cash.push(x);
            a++;
          }
        }
        if (a == 0) p = -1;
      }
      else {
        if (x == 100) {
          let a = 0, b = 0;
          for (let i = 0; i < cash.length; i++) {
            if (cash[i] == 50) { cash.splice(i, 1); a++; break; }
          }
          let k;
          if (a == 1) k = 1; else k = 3;
          for (let j = 0; j < k; j++)
            for (let i = 0; i < cash.length; i++) {
              if (cash[i] == 25) { cash.splice(i, 1); b++; }
            }
          if ((a == 1 && b == 1) || (b == 3)); else p = -1;
        }
      }
    }
    if (p == -1) return 'NO';
  }

  return 'YES';
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
